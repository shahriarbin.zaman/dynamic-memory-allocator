//SHAHRIAR BIN ZAMAN
//UCID: 30111988
//ASSIGNMENT: A6 memsim.cpp /



/// -------------------------------------------------------------------------------------
/// this is the only file you need to edit
/// -------------------------------------------------------------------------------------
///
/// (c) 2022, Pavol Federl, pfederl@ucalgary.ca
/// Do not distribute this file.

#include "memsim.h"
#include <cassert>
#include <iostream>
#include <unordered_map>
#include <list>
#include <set>
#include <iterator>
#include <climits>
#include <cmath>
#include <iomanip>



#define SPACES 20


// I recommend you implement the simulator as a class. This is only a suggestion.
// If you decide not to use this class, feel free to remove it.





struct Partition {
  int tag;
  int64_t size, addr;

  Partition(int tag,int64_t size, int64_t addr){
    //constructor
    this->tag = tag;
    this->size = size;
    this->addr = addr;

  }
};

typedef std::list<Partition>::iterator PartitionRef;
typedef std::set<PartitionRef>::iterator TreeRef;

struct scmp {
  bool operator()(const PartitionRef & c1, const PartitionRef & c2) const {
    if (c1->size == c2->size)
      return c1->addr < c2->addr;
    else return c1->size > c2->size;
  }
};



struct Simulator {
    int64_t page_size;
    uint32_t debug_i = 0;  //used for debugging
    int64_t n_pages_requested;
    std::list<Partition>all_blocks;
    std::set<PartitionRef, scmp>free_blocks;
    std::unordered_map<long, std::vector<PartitionRef>>tagged_blocks;

  Simulator(int64_t pagesize)
  {

   // constructor
    page_size = pagesize;
    n_pages_requested = 0;

  }


  void allocate(int tag, int size)
  {
    // Pseudocode for allocation request:
    // - search through the list of partitions from start to end, and
    //   find the largest partition that fits requested size
    //     - in case of ties, pick the first partition found
    // - if no suitable partition found:
    //     - get minimum number of pages from OS, but consider the
    //       case when last partition is free
    //     - add the new memory at the end of partition list
    //     - the last partition will be the best partition
    // - split the best partition in two if necessary
    //     - mark the first partition occupied, and store the tag in it
    //     - mark the second partition free

    //if(all_blocks.empty()){
    if(all_blocks.empty()){
      // this is the initial case when we have no partitions at all

      // create a new partition with the size to be multiple of the page size
      int64_t n_pages = ceil(double(size)/double(page_size));
      int64_t size_allocated = n_pages * page_size;
      // update page requested in results
      n_pages_requested = n_pages_requested + n_pages;

      // push back this partition to the all_blocks and free_blocks
      all_blocks.push_back({-1, size_allocated, 0});

      free_blocks.insert(all_blocks.begin());
    }




    // you can use a flag for if you have found a free partition or not
    bool found_free_partition = false;
    // use a different variable for required largest free partition
    PartitionRef required_largest_free_partition;

    if(!free_blocks.empty()){
      //TreeRef it = free_blocks.begin();

      // Iterator functionalities implemented from https://github.com/stephanedorotich/t457/blob/main/7-Memsim/list_iterators.cpp
      TreeRef it = free_blocks.begin();
      // this will give you an iterator to the largest free partition
      PartitionRef y = *it;
      // if this partition is big enough, we set the partition found as true
      if( y->size >= size){
        //we set the partition found as true
        found_free_partition = true;
        // and required largest free partition as free_blocks.begin() [y according to my code]
        required_largest_free_partition = y;

      }
    }

    //if(free partition is not found){
    if(found_free_partition == false){

      //if(last block in all_partions is free){
      if(std::prev(all_blocks.end())->tag == -1){

        // load pages, create an empty partition where the size is of the order of page size
        //  and merge with the last partition
        // change the address and size of this partition based on the last partition
        int64_t n_pages;

        if((size - std::prev(all_blocks.end())->size) % page_size == 0){
          n_pages = (size - std::prev(all_blocks.end())->size) / page_size;

        }
        else{
          n_pages = (size - std::prev(all_blocks.end())->size) / page_size + 1;

        }
        int64_t size_allocated = n_pages * page_size;
        n_pages_requested = n_pages_requested + n_pages;

        // last block
        PartitionRef last_block = std::prev(all_blocks.end());

        // enlarge the last block.
        // Step 1.
        //    Remove it from free_blocks
        free_blocks.erase(last_block);
        // Step 2.
        //    Update it's size. (size+=size_allocated)
        last_block->size += size_allocated;
        // since it is a free block, push it to free_blocks()
        free_blocks.insert(last_block);

      }else{ //last block is not free

        // simply add a new partition by loading from the memory
        // size is of the order of page size

        int64_t n_pages;
        if ((size % page_size) == 0){
            n_pages = size / page_size;
        }
        else{
            n_pages = size/page_size + 1;
        }
        int64_t size_allocated = n_pages * page_size;
        // update page requested in results
        n_pages_requested = n_pages_requested + n_pages;
        PartitionRef last_block = std::prev(all_blocks.end());

        int64_t addr2 = last_block->addr + last_block->size;
        // last block
        all_blocks.push_back({-1, size_allocated, addr2});
        free_blocks.insert(std::prev(all_blocks.end()));
        
      }

    //required free partition will be now the last partition of all_Partitions
    required_largest_free_partition = std::prev(all_blocks.end());
  }

  if(required_largest_free_partition->size > size){

    // find the largest_empty_partition from the tree
    TreeRef i = free_blocks.begin();
    PartitionRef m = *i;

    // erase from the tree
    free_blocks.erase(m);
    // allocate this partition to the process and add it to the tagged blocks
    all_blocks.insert(m,Partition(tag,size,m->addr));
    tagged_blocks[tag].push_back(std::prev(m));

    m->addr = m->addr + size;
    m->size = m->size - size;

   free_blocks.insert(m);


  }
  else{

    /* 
      Anshdeep Singh:
      this erase wont work, erase method of tree takes TreeRef not PartitionRef
      So, first you need to do 
      TreeRef tree_it = free_blocks.find(required_largest_free_partition);
      free_blocks.erase(tree_it);
    
    */

    TreeRef tree_it = free_blocks.find(required_largest_free_partition);
    free_blocks.erase(tree_it);

    PartitionRef y = *tree_it;

    y->tag = tag;

    tagged_blocks[tag].push_back(y);



  }



  }


  void deallocate(int tag)
  {
    // TA AMIR helped correct 
// Pseudocode for deallocation request:
    // - for every partition
    //     - if partition is occupied and has a matching tag:
    //         - mark the partition free
    //         - merge any adjacent free partitions

    std::vector<PartitionRef> partitions_with_tag = tagged_blocks[tag];
    PartitionRef current;

    //for(iterate through partitions_with_tag ){
    for (int i =0; i< (int)partitions_with_tag.size();i++){
      //current = partitions_with_tag[i];
      current = partitions_with_tag[i];
      // associate tag -1 with current
      current->tag = -1;


      //if(current!=all_blocks.begin() && std::prev(current)->tag==-1){
      if(current!=all_blocks.begin() && std::prev(current)->tag== -1){
        //find the previous of the current block
        // do a left merge

        current->size = current->size + std::prev(current)->size;
        current->addr = std::prev(current)->addr;


        // erase the previous of the current block from all blocks
	      free_blocks.erase(std::prev(current));
        all_blocks.erase(std::prev(current));

      }
      // if(current!=std::prev(all_blocks.end()) && std::next(current)->tag==-1){
      if(current!=std::prev(all_blocks.end()) && std::next(current)->tag==-1){

        // find the next of the current block
        // do a right merge
        current->size = current->size + std::next(current)->size;


        // erase next of the current block from all blocks 
        free_blocks.erase(std::next(current));
        all_blocks.erase(std::next(current));

      }

    // insert current block to free_blocks
    free_blocks.insert(current);
  }

  //erase all blocks associated with the tag in tagged_blocks

  tagged_blocks[tag].clear();

}
  
  
  MemSimResult getStats()
  {
    // let's guess the result... :)
    MemSimResult result;
    // result.max_free_partition_size = 123;
    // result.max_free_partition_address = 321;
    // result.n_pages_requested = 111;

    result.n_pages_requested = n_pages_requested;

    if(!free_blocks.empty()){

      //free blocks have information sorted in descending order
      result.max_free_partition_size = (*free_blocks.begin())->size;
      result.max_free_partition_address = (*free_blocks.begin())->addr;
    }
    else{
       result.max_free_partition_size = 0;
       result.max_free_partition_address = 0;
    }
    return result;
  }
  void check_consistency()
  {
    // mem_sim() calls this after every request to make sure all data structures
    // are consistent. Since this will probablly slow down your code, you should
    // disable comment out the call below before submitting your code for grading.
    check_consistency_internal();
  }
  void check_consistency_internal()
  {
    // you do not need to implement this method at all - this is just my suggestion
    // to help you with debugging

    // here are some suggestions for consistency checks (see appendix also):

    // make sure the sum of all partition sizes in your linked list is
    // the same as number of page requests * page_size

    // make sure your addresses are correct

    // make sure the number of all partitions in your tag data structure +
    // number of partitions in your free blocks is the same as the size
    // of the linked list

    // make sure that every free partition is in free blocks

    // make sure that every partition in free_blocks is actually free

    // make sure that none of the partition sizes or addresses are < 1

    std::cout << "\nIteration<" << debug_i++ << "> =======================================================\n";
        
    // you do not need to implement this method at all - this is just my suggestion
    // to help you with debugging

    // here are some suggestions for consistency checks (see appendix also):

    // make sure the sum of all partition sizes in your linked list is
    // the same as number of page requests * page_size
    int64_t total_size = 0;
    for (PartitionRef it = all_blocks.begin(); it != all_blocks.end(); it=std::next(it)) {
      total_size+= (*it).size;
    }
    int64_t expected_size = n_pages_requested * page_size;

    std::cout << std::setw(SPACES) << std::right << "Total Size: ";
    
    if (total_size == expected_size) {
      std::cout << "Pass\n";
    } else {
      std::cout << "Fail\n";
      std::cout << "\t\tExpected: " << expected_size << std::endl;
      std::cout << "\t\tActual:   " << total_size << std::endl;
    }

    // make sure your addresses are correct
    int64_t addr = 0;
    int64_t id = 0;
    std::cout << std::setw(SPACES) << std::right << "Addresses: ";
    for (PartitionRef it = all_blocks.begin(); it != all_blocks.end(); it=std::next(it)) {
      if ((*it).addr != addr) {
        std::cout << "Fail\n";
        std::cout << "\t\tBlock:    " << id << std::endl;
        std::cout << "\t\tExpected: " << addr+(*it).size << std::endl;
        std::cout << "\t\tActual:   " << (*it).addr << std::endl;
        id = -1;
        break;
      }
      id++;
      addr+=(*it).size;
    }
    if (id != -1) std::cout << "Pass\n";

    // make sure the number of all partitions in your tag data structure +
    // number of partitions in your free blocks is the same as the size
    // of the linked list

    // make sure that every free partition is in free blocks

    // make sure that every partition in free_blocks is actually free

    // make sure that none of the partition sizes or addresses are < 1
    std::cout << std::setw(SPACES) << std::right << "Ss and As (+): ";
    id = 0;
    for (PartitionRef it = all_blocks.begin(); it != all_blocks.end(); it=std::next(it)) {
      if ((*it).size < 1 || (*it).addr < 0) {
        std::cout << "Fail\n";
        std::cout << "\t\tBlock:    " << id << std::endl;
        std::cout << "\t\tSize:     " << (*it).size << std::endl;
        std::cout << "\t\tAddress:  " << (*it).addr << std::endl;
        id = -1;
      }
      id++;
    }
    if (id != -1)
      std::cout << "Pass\n";

  }
};

// re-implement the following function
// ===================================
// parameters:
//    page_size: integer in range [1..1,000,000]
//    requests: array of requests
// return:
//    some statistics at the end of simulation
MemSimResult mem_sim(int64_t page_size, const std::vector<Request> & requests)
{
  // if you decide to use the simulator class above, you probably do not need
  // to modify below at all
  Simulator sim(page_size);
  for (const auto & req : requests) {
    if (req.tag < 0) {
      sim.deallocate(-req.tag);
    } else {
      sim.allocate(req.tag, req.size);
    }
    //sim.check_consistency();
  }
  return sim.getStats();
}
